
var startHttpProxy = function(proxyReadyCB) {
    var isReachable = require('is-reachable');
    var config = require('./config.js').getConfig();
    var http = require('http');
    var httpProxy = require('http-proxy');

    function getWaraServerUrl() {
        return config.proxy.wara_url+':'+config.proxy.wara_port;
    }

    var proxy = httpProxy.createProxyServer({});

    proxy.on('error', function(err, req, res) {

    });

    proxy.on('proxyReq', function (proxyReq, req, res, options) {
        console.log('proxyReq......');
        proxyReq.setHeader(config.proxy.header.key, config.proxy.header.value);
    });

    var server = http.createServer(function(req, res) {

        console.log("start time: " + new Date());


        isReachable(getWaraServerUrl(), function(err, reachableResult) {
            if (!!reachableResult) {
                console.log("isReachable: " + new Date());
                proxy.web(req, res, {target: config.proxy.wara_url+':'+config.proxy.wara_port});

            } else {
                res.writeHead(200, {
                    'Content-type': 'application/json'
                });
                res.end();
                console.log("NOOOOT isReachable: " + new Date());
            }
        });




        console.log("end time: " + new Date());

    });

    server.on('error', function(err) {
        console.log('server error');
        console.log(err);
    });

    console.log("listening on port " + config.httpServer.port);
    server.listen(config.httpServer.port, function() {
        proxyReadyCB();
    });


}

exports.start = startHttpProxy;
