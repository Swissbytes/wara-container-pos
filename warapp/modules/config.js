var fs = require('fs');
var getConfig = function() {
  return JSON.parse(fs.readFileSync('./warapp/config/default.json'));
};

exports.getConfig = getConfig;
