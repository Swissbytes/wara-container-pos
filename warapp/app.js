var isReachable = require('is-reachable');
var config = require('./modules/config.js').getConfig();
 //or global.window.nwDispatcher.requireNwGui() (see https://github.com/rogerwang/node-webkit/issues/707)

 function createMenu() {



    var nw = require('nw.gui');

        // Window menu
        var windowMenu = new nw.Menu({ type: 'menubar'});


        if (windowMenu.createMacBuiltin) {
            windowMenu.createMacBuiltin("WARA POS");
        }


        console.log("menu..................");


        // Actions menu
        var actionsMenu = new nw.Menu();

        // Refresh sub-entry
        actionsMenu.append(new nw.MenuItem({
            label: 'Refrescar pagina',
            click: function() {
                reloadPage();
            }
        }));

        // View menu
        var viewMenu = new nw.Menu();

        // Developers tools sub-entry
        viewMenu.append(new nw.MenuItem({
            label: 'Developer tools',
            click: function() {
                nw.Window.get().showDevTools("wara-content");
            }
        }));

        // add to window menu
        windowMenu.append(new nw.MenuItem({
            label: 'Acciones',
            submenu: actionsMenu
        }));

        windowMenu.append(new nw.MenuItem({
            label: 'View',
            submenu: viewMenu
        }));



        // Assign to window
        nw.Window.get().menu = windowMenu;

    }

    function reloadPage() {
        showLoadingSpinner();
        isReachable(getWaraServerUrl(), function(err, reachableResult) {
            if (!!reachableResult) {
                var currentUrl = document.getElementById("wara-content").contentWindow.location.href;

                document.getElementById('wara-content').src = "";
                setTimeout(function() {
                    document.getElementById('wara-content').src = currentUrl;
                    setTimeout(function() {
                        hideLoadingSpinner();
                    }, 3000);

                }, 100);


            } else {
                hideLoadingSpinner();
                alert('No se puede recargar la pagina, verifique la conexion a Internet');
            }
        });
    }

    function getWaraServerUrl() {
        return config.proxy.wara_url+':'+config.proxy.wara_port;
    }

    function hideLoadingSpinner() {
        document.getElementById('loading-spinner').style.display = "none";
    }

    function showLoadingSpinner() {
        document.getElementById('loading-spinner').style.display = "block";
    }

    function proxyReady() {
        console.log('proxyReady......');
        document.getElementById('wara-content').src = config.httpServer.url + ":" + config.httpServer.port;
        document.getElementById('wara-content').style.display = "block";
        document.getElementById('conexion-fail').style.display='none';
        setTimeout(function() {
            hideLoadingSpinner();
        }, 3000);
    }

    function bootstrapWaraPos() {
        showLoadingSpinner();
        isReachable(getWaraServerUrl(), function(err, reachableResult) {
            if (!!reachableResult) {

                // document.getElementById('conexion').style.display='none';

                require('./modules/proxy.js').start(proxyReady);
                createMenu();
            } else {
                console.log(String(reachableResult));
                document.getElementById('conexion-fail').style.display='block';
                hideLoadingSpinner();
            }
        });
    };

// auto start
    bootstrapWaraPos();

